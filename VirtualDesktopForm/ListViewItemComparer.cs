﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using IWshRuntimeLibrary;
using System.Collections;

namespace VirtualDesktopForm
{
    public class ListViewItemComparer : IComparer
    {

        public int col;
        public SortOrder order;
        public ListViewItemComparer()
        {
            col = 0;
            order = SortOrder.Ascending;
        }
        public ListViewItemComparer(int column, SortOrder order)
        {
            col = column;
            this.order = order;
        }
        public int Compare(object x, object y)
        {
            int returnVal = -1;
            if (col == 0)
            {
                returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                ((ListViewItem)y).SubItems[col].Text);
                // Determine whether the sort order is descending.
                if (order == SortOrder.Descending)
                    // Invert the value returned by String.Compare.
                    returnVal *= -1;
            }
            else
            {
                DateTime dtData1 = Convert.ToDateTime(((ListViewItem)x).SubItems[col].Text);
                DateTime dtData2 = Convert.ToDateTime(((ListViewItem)y).SubItems[col].Text);
                returnVal = DateTime.Compare(dtData1, dtData2);
                if (order == SortOrder.Descending)
                    // Invert the value returned by String.Compare.
                    returnVal *= -1;
            }


            return returnVal;
        }

    }
}
