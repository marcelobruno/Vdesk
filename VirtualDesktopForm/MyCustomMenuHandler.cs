﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Forms;

namespace VirtualDesktopForm
{
    
    class MyCustomMenuHandler : IContextMenuHandler
    {
        public static int nCopyImage;
        
        pictureViewer pvForm;

        public MyCustomMenuHandler(pictureViewer form)
        {
            pvForm = form;
            nCopyImage = 0;
        }


        public void OnBeforeContextMenu(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, IMenuModel model)
        {
            
            Console.WriteLine("Context menu opened !");

            // You can add a separator in case that there are more items on the list
            if (model.Count > 0)
            {
                model.AddSeparator();
            }


            // Add a new item to the list using the AddItem method of the model
            model.AddItem((CefMenuCommand)26501, "Show DevTools");
            model.AddItem((CefMenuCommand)26502, "Close DevTools");



            // Add another example item
            if (parameters.HasImageContents && parameters.SourceUrl != "")
            {
                // Add a separator
                model.AddSeparator();
                model.AddItem((CefMenuCommand)26503, "Save Image");
                model.AddItem((CefMenuCommand)26505, "Copy Image To Clipboard");
                
                
            }
            if( parameters.LinkUrl != "")
            {
                model.AddItem((CefMenuCommand)26504, "Copy link address");
                
            }

            //throw new NotImplementedException();
        }

        public bool OnContextMenuCommand(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, CefMenuCommand commandId, CefEventFlags eventFlags)
        {
            if (commandId == (CefMenuCommand)26501)
            {
                browser.GetHost().ShowDevTools();
                return true;
            }

            // React to the second ID (show dev tools method)
            if (commandId == (CefMenuCommand)26502)
            {
                browser.GetHost().CloseDevTools();
                return true;
            }

            // React to the third ID (Display alert message)
            if (commandId == (CefMenuCommand)26503) //Save Image
            {
                //frame.SelectAll();
                //frame.ExecuteJavaScriptAsync("document.execCommand('copy');");
                //pvForm.chromeBrowser3.GetFocusedFrame().Copy();
                //MessageBox.Show(frame.ToString());
                browser.GetHost().StartDownload(parameters.SourceUrl);
                //Type x = parameters.GetType();
            }

            if( commandId == (CefMenuCommand)26504) //Copy Link Address
            {
                Clipboard.SetText(parameters.LinkUrl);
            }
            if (commandId == (CefMenuCommand)26505) //Copy Image To Clipboard
            {
                  
                nCopyImage = 1;

                browser.GetHost().StartDownload(parameters.SourceUrl);

                

                
            }
            // Return false should ignore the selected option of the user !
            return false;
        }

        public void OnContextMenuDismissed(IWebBrowser browserControl, IBrowser browser, IFrame frame)
        {
            //throw new NotImplementedException();
        }






        bool IContextMenuHandler.RunContextMenu(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, IMenuModel model, IRunContextMenuCallback callback)
        {
            return false;//throw new NotImplementedException();
        }


    }
}
