﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VirtualDesktopForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //if (/* Main 아래에 정의된 함수 */IsAdministrator() == false)
            //{
            //    try
            //    {
            //        ProcessStartInfo procInfo = new ProcessStartInfo();
            //        procInfo.UseShellExecute = true;
            //        procInfo.FileName = Application.ExecutablePath;
            //        procInfo.WorkingDirectory = Environment.CurrentDirectory;
            //        procInfo.Verb = "runas";
            //        Process.Start(procInfo);
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message.ToString());
            //    }
            //    return;
            //}

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    //    public static bool IsAdministrator()
    //  {
    //      WindowsIdentity identity = WindowsIdentity.GetCurrent();
    //      if (null != identity)
    //      {
    //          WindowsPrincipal principal = new WindowsPrincipal(identity);
    //          return principal.IsInRole(WindowsBuiltInRole.Administrator);
    //      }
    //      return false;
    //    }

    }
}
