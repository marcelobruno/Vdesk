﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Forms;

namespace VirtualDesktopForm
{
    class MyDownloadHandler : IDownloadHandler
    {
        pictureViewer myForm;
        

        public MyDownloadHandler(pictureViewer form)
        {
            myForm = form;
        }
        public void OnBeforeDownload(IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
        {
            if (!callback.IsDisposed)
            {
                using (callback)
                {
                    myForm.UpdateDownloadItem(downloadItem);
                    if (MyCustomMenuHandler.nCopyImage != 1)
                        callback.Continue(downloadItem.SuggestedFileName, showDialog: true);
                    else
                    {
                        
                        callback.Continue(myForm.szTempImage, showDialog: false);
                        myForm.timer.Enabled = true;
                    }
                    MyCustomMenuHandler.nCopyImage = 0;
                }
            }
        }

        public void OnDownloadUpdated(IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
        {
            myForm.UpdateDownloadItem(downloadItem);
            if (downloadItem.IsInProgress && myForm.CancelRequests.Contains(downloadItem.Id)) callback.Cancel();
            //Console.WriteLine(downloadItem.Url + " %" + downloadItem.PercentComplete + " complete");
            Console.WriteLine(downloadItem.PercentComplete);
        }

    }
}
