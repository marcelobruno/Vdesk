﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using System.Net;

namespace VirtualDesktopForm
{

    public partial class pictureViewer : Form
    {
        private string startURL = "https://www.google.com/";

        private Dictionary<int, DownloadItem> downloads;
        private Dictionary<int, string> downloadNames;
        private List<int> downloadCancelRequests;
        public Timer timer;
        public string szTempImage;
        public TabControlExtra tabActive;
        public pictureViewer()
        {
            InitializeComponent();
            InitializeChromium();
            timer = new Timer();
            szTempImage = System.IO.Path.GetTempPath();
            szTempImage += "clipboard.img";
            if (File.Exists(szTempImage))
                File.Delete(szTempImage);
            timer.Interval = 50;
            timer.Tick += new EventHandler(timer_Elapsed);
            timer.Enabled = false;
            tabActive = tabControlExtra1;
            
        }
        public List<int> CancelRequests
        {
            get
            {
                return downloadCancelRequests;
            }
        }
        private void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            //settings.CefCommandLineArgs.Add("proxy-server", "127.0.0.1:8182");
            // Initialize Cef with provided settings
            settings.CachePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CEF";
            

            // Create a browser component
            //chromeBrowser = new ChromiumWebBrowser("http://www.youtube.com");
            try
            {
                Cef.Initialize(settings);
                
                //chromeBrowser = new ChromiumWebBrowser("http://web.whatsapp.com");
                //chromeBrowser2 = new ChromiumWebBrowser("http://www.gmail.com");
                //chromeBrowser3 = new ChromiumWebBrowser("http://www.google.com");
                //chromeBrowser4 = new ChromiumWebBrowser("https://www.daz3d.com/paportal/portal/index/");
                //chromeBrowser.MenuHandler = new MyCustomMenuHandler(this);
                //chromeBrowser.DownloadHandler = new MyDownloadHandler(this);
                //chromeBrowser2.MenuHandler = new MyCustomMenuHandler(this);
                //chromeBrowser2.DownloadHandler = new MyDownloadHandler(this);
                //chromeBrowser3.MenuHandler = new MyCustomMenuHandler(this);
                //chromeBrowser3.DownloadHandler = new MyDownloadHandler(this);
                //chromeBrowser4.MenuHandler = new MyCustomMenuHandler(this);
                //chromeBrowser4.DownloadHandler = new MyDownloadHandler(this);
                downloadCancelRequests = new List<int>();
                downloads = new Dictionary<int, DownloadItem>();
                downloadNames = new Dictionary<int, string>();
                //add...
                //this.Controls.Add(chromeBrowser);
                //toolStripContainer1.ContentPanel.Controls.Add(chromeBrowser);
                //tabPage1.Controls.Add(chromeBrowser);
                //tabPage2.Controls.Add(chromeBrowser2);
                //tabPage3.Controls.Add(chromeBrowser3);
                //tabPage4.Controls.Add(chromeBrowser4);
                //tabControlExtra1.TabPages[0].Text = startURL;
                //AddNewBrowser(tabControlExtra1.TabPages[0], startURL);

                //chromeBrowser.Dock = DockStyle.Fill;
            }
            catch { }

        }
        public void UpdateDownloadItem(DownloadItem item)
        {
            lock (downloads)
            {
                //SuggestedFileName comes full only in the first attempt so keep it somewhere
                if (item.SuggestedFileName != "") downloadNames[item.Id] = item.SuggestedFileName;

                //Set it back if it is empty
                if (item.SuggestedFileName == "" && downloadNames.ContainsKey(item.Id)) item.SuggestedFileName = downloadNames[item.Id];

                downloads[item.Id] = item;
            }
        }
        private void MyBrowserOnFrameLoadEnd(object sender, FrameLoadEndEventArgs frameLoadEndEventArgs)
        {
            //chromeBrowser.SetZoomLevel(2.0);
        }
        private void pictureViewer_Load(object sender, EventArgs e)
        {
            //string theURI = "https://mail.google.com/mail/u/0/#inbox";
            //string theFrame = "";
            //System.Text.ASCIIEncoding anEncoder = new ASCIIEncoding();
            //byte[] HTTPPostData = anEncoder.GetBytes("X=123&Y=456.789");
            //string additionalHeaders = "Content-Type: application/x-www-form-urlencoded\n\r";
            //webBrowser1.Navigate(theURI, theFrame, HTTPPostData, additionalHeaders);
            //tabControl1.SelectTab(1);
            //tabControl1.SelectTab(0);


        }

        private void pictureViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Cef.Shutdown();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {

            ((ChromiumWebBrowser)tabActive.SelectedTab.Controls[0]).Load(txtBoxUrl.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //chromeBrowser.SetZoomLevel(2.0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //chromeBrowser.SetZoomLevel(0.1);
        }

        private void navigateUrl()
        {
            
            if (this.txtActiveTab.Text == "tab1_0")
            {
                //chromeBrowser.Load(this.txtBoxUrl.Text);
            }
            if (this.txtActiveTab.Text == "tab1_1")
            {
                //chromeBrowser2.Load(this.txtBoxUrl.Text);
            }

        }

        private void txtBoxUrl_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(e.KeyCode.ToString());
            if (e.KeyCode.ToString() == "Return")
            {
                navigateUrl();
            }
        }

        //private void tabControl1_Click(object sender, EventArgs e)
        //{
        //    //MessageBox.Show(this.tabControl1.SelectedIndex.ToString());
        //    //txtActiveTab.Text = "tab1_" + tabControl1.SelectedIndex.ToString();
        //}

        //private void tabControl2_Click(object sender, EventArgs e)
        //{
        //    txtActiveTab.Text = "tab2_" + tabControl2.SelectedIndex.ToString();
        //}

        //private void tabPage2_Click(object sender, EventArgs e)
        //{
        //    txtActiveTab.Text = "tab1_" + tabControl1.SelectedIndex.ToString();
        //}

        private void button3_Click(object sender, EventArgs e)
        {
            //tabControl1.Width = tabControl1.Width - 100;
            //tabControl2.Width = tabControl2.Width + 100;
            //tabControl2.Left = tabControl2.Left - 100;
            //chromeBrowser3.Find(0, "Google", true, false, false);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //tabControl1.Width = tabControl1.Width + 100;
            //tabControl2.Width = tabControl2.Width - 100;
            //tabControl2.Left = tabControl2.Left + 100;
            
            //for (int i = 0; i < tabControl2.TabCount ; i++)
            //{
            //    Button button = new Button();
            //    button.Name = "btn_" + i.ToString();
            //    button.Text = i.ToString();
            //    button.Text = tabControl2.TabPages[i].Text.ToString();
            //    flowLayoutPanel2.Controls.Add(button);
            //}
        }
        public void timer_Elapsed(object sender, EventArgs e)
        {
            if (File.Exists(this.szTempImage))
            {
                try
                {
                    
                    FileStream fsIn = new FileStream(this.szTempImage, FileMode.Open, FileAccess.Read, FileShare.Read);
                    Image iImage = Image.FromStream(fsIn);
                    Clipboard.SetImage(iImage);
                    fsIn.Close();
                    File.Delete(this.szTempImage);
                    timer.Enabled = false;
                }
                catch
                {

                }


            }

        }
        public void InvokeOnUiThreadIfRequired(Action action)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(action);
            }
            else
            {
                action.Invoke();
            }
        }
        private void Browser_TitleChanged(object sender, TitleChangedEventArgs e)
        {
            InvokeOnUiThreadIfRequired(() =>
            {
                TabPage tabStrip = (TabPage)((ChromiumWebBrowser)sender).Parent;
                TabControlExtra tabCur = (TabControlExtra)tabStrip.Parent;

                //string szTemp = e.Title + "                            ";

                //tabStrip.Text = szTemp.Substring(0, 18);
                if (e.Title.Length > 6)
                    tabStrip.Text = "121312dfgd";
                else
                    tabStrip.Text = "      ";
                tabStrip.ToolTipText = e.Title;
                WebClient wc = new WebClient();
                ChromiumWebBrowser cromeBrowser = (ChromiumWebBrowser)sender;
                string szAddress = cromeBrowser.Address.ToString();
                tabStrip.Tag = szAddress;
                try
                {
                    MemoryStream memorystream =
                        new MemoryStream(wc.DownloadData("http://" + new Uri(szAddress).Host + "/favicon.ico"));
                    Icon icon = new Icon(memorystream);
                    tabCur.ImageList.Images.Add(icon.ToBitmap());
                    tabStrip.ImageIndex = tabCur.ImageList.Images.Count - 1; // sets favicon in current tab
                }
                catch { }
                txtBoxUrl.Text = szAddress;
            });
        }
        private ChromiumWebBrowser AddNewBrowser(TabPage tabStrip, String url)
        {
            if (url == "") url = startURL;
            
            ChromiumWebBrowser browser = new ChromiumWebBrowser(url);
            browser.Dock = DockStyle.Fill;
            tabStrip.Controls.Add(browser);
            browser.BringToFront();
            //browser.StatusMessage += Browser_StatusMessage;
            //browser.LoadingStateChanged += Browser_LoadingStateChanged;
            browser.TitleChanged += Browser_TitleChanged;
            //browser.LoadError += Browser_LoadError;
            //browser.AddressChanged += Browser_AddressChanged;
            browser.DownloadHandler = new MyDownloadHandler(this);
            browser.MenuHandler = new MyCustomMenuHandler(this);
            //browser.LifeSpanHandler = lHandler;
            //browser.KeyboardHandler = kHandler;
            //if (url.StartsWith("chrome:"))
            //{
            //    browser.RegisterAsyncJsObject("host", host, true);
            //}
            return browser;
        }
        public ChromiumWebBrowser AddNewBrowserTab(String url, bool focusNewTab = true)
        {
            return (ChromiumWebBrowser)this.Invoke((Func<ChromiumWebBrowser>)delegate
            {
                TabPage tabStrip = new TabPage("blank");
                tabStrip.Text = startURL;
                //tabStrip.Name = "ddfaf";


                //tabControlExtra1.TabPages.Add(tabStrip);
                if (tabControlExtra1.SelectedIndex >= 0)
                    tabControlExtra1.SelectedIndex--;
                tabControlExtra1.TabPages.Insert(tabControlExtra1.TabPages.Count - 1, tabStrip);
                tabControlExtra1.SelectedIndex = tabControlExtra1.TabPages.Count - 2;
                //TabPage tabTemp = tabControlExtra1.TabPages[tabControlExtra1.TabPages.Count - 1];

                //newStrip = tabStrip;
                ChromiumWebBrowser browser = AddNewBrowser(tabStrip, url);
                //if (focusNewTab) timer1.Enabled = true;
                return browser;
            });
        }

        private void tabPages_tabItemSelectionChanged(object sender, EventArgs e)
        {

        }





        private void pictureViewer_OnNewTabEvent(TabPage page)
        {
            
            AddNewBrowser(page, startURL);
        }

        private void tabControlExtra1_OnCloseTabEvent(int index)
        {
            //imageList1.Images.RemoveAt(index);
        }



        private void tabControlExtra2_OnFocus(object sender, EventArgs e)
        {
            tabActive = tabControlExtra2;
            if(tabActive.SelectedTab != null)
                txtBoxUrl.Text = (string)tabActive.SelectedTab.Tag;
        }

        private void tabControlExtra1_OnFocus(object sender, EventArgs e)
        {
            tabActive = tabControlExtra1;
            if(tabActive.SelectedTab != null)
                txtBoxUrl.Text = (string)tabActive.SelectedTab.Tag;
        }

        private void txtBoxUrl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if( e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                ((ChromiumWebBrowser)tabActive.SelectedTab.Controls[0]).Load(txtBoxUrl.Text);
            }
        }

        private void TabControl_SelChanged(object sender, EventArgs e)
        {
            TabPage tab = ((TabControlExtra)sender).SelectedTab;
            if ( tab != tabStripAdd )
            {
                txtBoxUrl.Text = (string)tab.Tag;
            }
        }
    }
}
