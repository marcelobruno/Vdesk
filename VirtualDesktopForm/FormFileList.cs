﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using IWshRuntimeLibrary;
using System.Collections;

namespace VirtualDesktopForm
{
    public partial class FormFileList : Form
    {
        private int sortColumn = -1;
        private List<string> listFullPath;
        public FormFileList()
        {
            InitializeComponent();
            listFullPath = new List<string>();

        }
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != sortColumn)
            {
                // Set the sort column to the new column.
                sortColumn = e.Column;
                // Set the sort order to ascending by default.
                listView1.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (listView1.Sorting == SortOrder.Ascending)
                    listView1.Sorting = SortOrder.Descending;
                else
                    listView1.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            listView1.Sort();
            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            this.listView1.ListViewItemSorter = new ListViewItemComparer(e.Column,
                                                              listView1.Sorting);
        }
        private void FormFileList_Load(object sender, EventArgs e)
        {
            
            ShowRecentDosToListView();

        }
        private void ShowRecentDosToListView()
        {
            listView1.Items.Clear();
            listFullPath.Clear();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Recent);
            var files = System.IO.Directory.EnumerateFiles(path);
            IEnumerator<string> num = files.GetEnumerator();

            string Temp;
            int nIdx = 0;
            Icon ico;
            ListViewItem lvi;
            while (num.MoveNext())
            {
                string str = num.Current;

                if (str.IndexOf(".lnk") < 0)
                    continue;
                WshShell shell = new WshShell();
                IWshShortcut link = (IWshShortcut)shell.CreateShortcut(str);
                //Temp = GetShortcutTarget(str);
                Temp = link.TargetPath;

                if (Temp != "")
                {
                    FileInfo fileInfo = new FileInfo(Temp);
                    if ((fileInfo.Attributes & FileAttributes.Directory) != FileAttributes.Directory)
                    {
                        ico = Icon.ExtractAssociatedIcon(Temp);
                        this.imageList1.Images.Add(ico);
                        if (chkFullPath.CheckState == CheckState.Checked)
                            lvi = new ListViewItem(Temp);
                        else
                            lvi = new ListViewItem(fileInfo.Name);
                        lvi.Tag = Temp;
                        lvi.SubItems.Add(fileInfo.LastWriteTime.ToString());
                        lvi.ImageIndex = nIdx;
                        listView1.Items.Add(lvi);
                        

                        nIdx++;
                    }
                    //Console.WriteLine(value);
                }
            }

            this.listView1.ListViewItemSorter = new ListViewItemComparer(1,
                                                  SortOrder.Descending);
            this.listView1.Sort();
        }
        private void listView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            List<string> selection = new List<string>();

            foreach (ListViewItem item in listView1.SelectedItems)
            {
                int imgIndex = item.ImageIndex;
                selection.Add((string)item.Tag);
            }

            DataObject data = new DataObject(DataFormats.FileDrop, selection.ToArray());
            DoDragDrop(data, DragDropEffects.Copy);
        }

        private void chkFullPath_CheckedChanged(object sender, EventArgs e)
        {


            ShowRecentDosToListView();

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
