﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
namespace VirtualDesktopForm
{
    public partial class FormFoldersXML : Form
    {
        //const string ITEM_TAG_NAME = " [item]";


        public FormFoldersXML()
        {
            InitializeComponent();
            treeView1.ShowNodeToolTips = true;
            treeView1.HideSelection = true;
            
            //btnAddCategory.Enabled = false;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void addTreeNode(XmlNode xmlNode, TreeNode treeNode)
        {
            XmlNode xNode;
            TreeNode tNode;
            XmlNodeList xNodeList;
            int nIconIndex;
            if (xmlNode.HasChildNodes) //The current node has children
            {
                xNodeList = xmlNode.ChildNodes;
                if (xmlNode.Name == "itemName")
                {
                    if(xNodeList.Count > 0)
                        treeNode.Text = xNodeList[0].Value;
                    
                    XmlAttributeCollection xmlAttribList = xmlNode.Attributes;
                    if(xmlAttribList.Count > 0)
                        treeNode.ToolTipText = xmlAttribList[0].Value;
                }
                else
                {
                    for (int x = 0; x <= xNodeList.Count - 1; x++) //Loop through the child nodes
                    {
                        xNode = xmlNode.ChildNodes[x];
                        if(xNode.Name == "itemName")
                            nIconIndex = 1;
                        else
                            nIconIndex = 0;
                        treeNode.Nodes.Add(new TreeNode(xNode.Name, nIconIndex, nIconIndex));
                        tNode = treeNode.Nodes[x];
                        addTreeNode(xNode, tNode);
                    }
                }
            }
            //else //No children, so add the outer xml (trimming off whitespace)
            //    treeNode.Text = xmlNode.OuterXml.Trim();
            
        }
        private void btnXmlOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open XML Document";
            dlg.Filter = "XML Files (*.xml)|*.xml";
            dlg.FileName = Application.StartupPath + "\\..\\..\\example.xml";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtXmlName.Text = dlg.FileName;
                XmlLoadProc(dlg.FileName);
                //try
                //{
                //    //Just a good practice -- change the cursor to a wait cursor while the nodes populate
                //    this.Cursor = Cursors.WaitCursor;

                //    //First, we'll load the Xml document
                //    XmlDocument xDoc = new XmlDocument();
                //    xDoc.Load(dlg.FileName);

                //    //Now, clear out the treeview, and add the first (root) node
                //    treeView1.Nodes.Clear();
                //    treeView1.Nodes.Add(new TreeNode(xDoc.DocumentElement.Name, 0, 0));
                //    TreeNode tNode = new TreeNode();
                //    tNode = (TreeNode)treeView1.Nodes[0];

                //    //We make a call to AddNode, where we'll add all of our nodes
                //    addTreeNode(xDoc.DocumentElement, tNode);

                //    //Expand the treeview to show all nodes
                //    treeView1.ExpandAll();


                //}
                //catch (XmlException xExc) //Exception is thrown is there is an error in the Xml
                //{
                //    MessageBox.Show(xExc.Message);
                //}
                //catch (Exception ex) //General exception
                //{
                //    MessageBox.Show(ex.Message);
                //}
                //finally
                //{
                //    this.Cursor = Cursors.Default; //Change the cursor back
                //}
            }
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            string szCategory = txtCategroyName.Text;
            if (szCategory.Length > 0)
            {

                    if (treeView1.SelectedNode != null && treeView1.SelectedNode.ImageIndex == 0)
                    {
                        treeView1.SelectedNode.Nodes.Add(szCategory, szCategory, 0);
                        treeView1.ExpandAll();
                    }
                    else
                        treeView1.Nodes.Add(szCategory, szCategory, 0);
            }
            else
                MessageBox.Show("Please Input the Category Name String.");
        }

        private void txtCategroyName_TextChanged(object sender, EventArgs e)
        {
            //if (txtCategroyName.Text.Length > 0)
            //    btnAddCategory.Enabled = true;
            //else
            //    btnAddCategory.Enabled = false;
        }
        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            TreeNode tNode = treeView1.GetNodeAt(treeView1.PointToClient(Control.MousePosition));

            if (tNode == null)
            {
                if (prevNode != null)
                    prevNode.BackColor = treeView1.BackColor;
                treeView1.SelectedNode = null;
            }
            else
            {
                //tNode.BackColor = treeView1.BackColor;
                //treeView1.SelectedNode = null;
            }
        }
        private void treeView1_Click(object sender, MouseEventArgs e)
        {
            //if (treeView1.Nodes.Count == 0)
            //    return;

            if (e.Button == MouseButtons.Right )
            {
                if (treeView1.SelectedNode != null)
                {
                    treeView1.SelectedNode.BackColor = treeView1.BackColor;
                    prevNode = treeView1.SelectedNode = null;
                }
                return;
            }
            treeView1.SelectedNode = treeView1.GetNodeAt(treeView1.PointToClient(Control.MousePosition));

            if (treeView1.SelectedNode != null)
            {
                if (prevNode != null)
                    prevNode.BackColor = treeView1.BackColor;
                treeView1.SelectedNode.BackColor = SystemColors.Highlight;
                prevNode = treeView1.SelectedNode;
                txtItemName.Text = txtItemPath.Text = "";
                if (treeView1.SelectedNode.ImageIndex == 1)
                {

                    txtItemName.Text = treeView1.SelectedNode.Text;

                    txtItemPath.Text = treeView1.SelectedNode.ToolTipText;

                    btnAddCategory.Enabled = false;
                }
                else
                {
                    btnAddCategory.Enabled = true;
                }
            }
            else
            {
                if(prevNode != null)
                    prevNode.BackColor = treeView1.BackColor;
            }  
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
                treeView1.SelectedNode.Remove();
            else
                MessageBox.Show("Select a Item in TreeView");
        }
        TreeNode prevNode;
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {



        }
        private void treeView1_DblClick(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                if (treeView1.SelectedNode.ImageIndex== 1)
                {
                    MessageBox.Show(treeView1.SelectedNode.ToolTipText);
                }
            }
        }
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            TreeNode tNode;
            if ( txtItemName.Text.Length > 0)
            {
                    if (treeView1.SelectedNode != null)
                    {
                        if( treeView1.SelectedNode.ImageIndex == 0 )
                        {
                            tNode = treeView1.SelectedNode.Nodes.Add(txtItemName.Text, txtItemName.Text, 1, 1);
                        }
                        else
                            return;
                    }
                    else
                        tNode = treeView1.Nodes.Add(txtItemName.Text, txtItemName.Text, 1, 1);
                    tNode.ToolTipText = txtItemPath.Text;
                treeView1.ExpandAll();
            }
            else
                MessageBox.Show("Please input the Item Name String.");
        }

        private void FormFoldersXML_Shown(object sender, EventArgs e)
        {
            string FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\formfolders.xml";
            FileStream fs;
            if (!File.Exists(FileName))
            {

                fs = File.Create(FileName);
                fs.Close();
                return;
            }
             fs = File.Open(FileName, FileMode.Open);
            long nFileLen = fs.Length;
            fs.Close();
            if (nFileLen == 0)
                return;
            XmlLoadProc(FileName);

        }
        private void XmlLoadProc(string szFileName)
        {
            try
            {

                //Just a good practice -- change the cursor to a wait cursor while the nodes populate

                this.Cursor = Cursors.WaitCursor;

                //First, we'll load the Xml document
                XmlDocument xDoc = new XmlDocument();
                
                xDoc.Load(szFileName);

                //Now, clear out the treeview, and add the first (root) node
                treeView1.Nodes.Clear();
                //treeView1.Nodes.Add(new TreeNode(xDoc.DocumentElement.Name, 0, 0));
                TreeNode tNode = new TreeNode();
                //tNode = (TreeNode)treeView1.Nodes[0];
                foreach (XmlNode xNode in xDoc.DocumentElement)
                {
                    int nIconIndex;
                    if (xNode.Name == "itemName")
                        nIconIndex = 1;
                    else
                        nIconIndex = 0;
                    tNode = treeView1.Nodes.Add(xNode.Name, xNode.Name, nIconIndex, nIconIndex);
                    //We make a call to AddNode, where we'll add all of our nodes
                    addTreeNode(xNode, tNode);
                }

                
                //Expand the treeview to show all nodes
                treeView1.ExpandAll();
                

            }
            catch (XmlException xExc) //Exception is thrown is there is an error in the Xml
            {
                MessageBox.Show(xExc.Message);
            }
            catch (Exception ex) //General exception
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default; //Change the cursor back
            }
        }
        private void btnUpdateItem_Click(object sender, EventArgs e)
        {
            if (txtItemName.Text.Length > 0)
            {
                if (treeView1.SelectedNode != null && treeView1.SelectedNode.ImageIndex == 1)
                {
                    treeView1.SelectedNode.Text = txtItemName.Text;
                    treeView1.SelectedNode.ToolTipText = txtItemPath.Text;
                }
                else
                    MessageBox.Show("Please select an Item exactly to update.");

            }
            else
                MessageBox.Show("Please input the Item Name String.");
        }
        private void treeView1_LostFocus(object sender, EventArgs e)
        {
            //this.treeView1.SelectedNode.EnsureVisible();
            
        }
        private void btnSaveToXml_Click(object sender, EventArgs e)
        {
            serializeTreeview();
        }
        private void serializeTreeview()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            if (treeView1.Nodes.Count > 0)
            {
                dlg.FileName = this.treeView1.Nodes[0].Text + ".xml";
                dlg.Filter = "XML Files (*.xml)|*.xml";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    exportToXml(treeView1, dlg.FileName);
                }
            }
            else
                MessageBox.Show("Nothing Contents in Treeview");
        }
        //We use this in the export and the saveNode functions, though it's only instantiated once.
        private StreamWriter sr;
        public void exportToXml(TreeView tv, string filename)
        {
            sr = new StreamWriter(filename, false, System.Text.Encoding.UTF8);
            //Write the header
            sr.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            //Write our root node
            sr.WriteLine("<root>");
            foreach (TreeNode node in tv.Nodes)
            {
                sr.WriteLine("<" + treeView1.Nodes[0].Text + ">");
                saveNode(node.Nodes);
                //Close the root node
                sr.WriteLine("</" + treeView1.Nodes[0].Text + ">");
            }
            sr.WriteLine("</root>");
            sr.Close();
        }
        private void saveNode(TreeNodeCollection tnc)
        {
            foreach (TreeNode node in tnc)
            {
                //If we have child nodes, we'll write a parent node, then iterrate through
                //the children
                if (node.ImageIndex == 0)
                {
                    sr.WriteLine("<" + node.Text + ">");
                    saveNode(node.Nodes);
                    sr.WriteLine("</" + node.Text + ">");
                }
                else //No child nodes, so we just write the text
                {
                    string szXmlElement = "";
                    if (node.ImageIndex == 1)
                    {
                        szXmlElement = "<itemName";
                        if (node.ToolTipText != "")
                            szXmlElement += " path=\"" + node.ToolTipText + "\"";
                        szXmlElement += ">" + node.Text + "</itemName>";

                    }
                    //else
                    //    szXmlElement = node.Text;
                     sr.WriteLine(szXmlElement);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\formfolders.xml";
            exportToXml(treeView1, FileName);
            MessageBox.Show("formfolders.xml File Creation Succeeded!");
        }

        private void FormFoldersXML_Load(object sender, EventArgs e)
        {

        }

    }
}
